package ee.datanor.vali.it.leiabugi.service;

import ee.datanor.vali.it.leiabugi.model.Person;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@Slf4j
@Service
public class PersonService {

    List<Person> personsList = new ArrayList<>();

    public List<Person> getPersonsList() {
        return personsList;
    }

    public void setPersonsList(List<Person> personsList) {
        this.personsList = personsList;
    }

    public final Logger log = LoggerFactory.getLogger(getClass());

    @PostConstruct
    public void init() {
        Person firstP = new Person();
        firstP.setFirstName("Miki");
        firstP.setLastName("Hiir");
        firstP.setSocialSecurityId(31012340001L);

        Person secondP = new Person();
        secondP.setFirstName("Hagar");
        secondP.setLastName("Hirmus");
        secondP.setSocialSecurityId(34012340002L);

        Person thridP = new Person();
        thridP.setFirstName("Pipi");
        thridP.setLastName("Pikksukk");
        thridP.setSocialSecurityId(48012340003L);

        personsList.add(firstP);
        personsList.add(secondP);
        personsList.add(thridP);
        log.info("init done");
    }


    public Person getPerson(Long socialSecurityId) {
        log.info("personList size {} {}", personsList.size(), socialSecurityId);

        Optional<Person> person = personsList.stream().filter((p -> p.getSocialSecurityId() == socialSecurityId)).findAny();
        if (person.isPresent()) {
            return person.get();
        }
        return new Person();
    }


    public void addPerson(Person person) {
        if (person.getFirstName() != null && person.getLastName() != null && person.getSocialSecurityId() != 0)

            personsList.add(person);
    }

    public List<Person> getPersons() {
        return personsList;
    }

    public void updatePerson(Person person) {

        for (Person p: personsList) {
            if (p.getSocialSecurityId() == person.getSocialSecurityId()) {
                personsList.add(person);
                personsList.remove(p);

            }

        }
    }
}